#include <iostream>
#include <fstream>
#include <sstream>

#include <iterator>

#include <string>
#include <vector>
#include <set>

#include <stdexcept>

#include <cstdio>
#include <cstdlib>

#include <unistd.h>
#include <fcntl.h>

#include <getopt.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/wait.h>

#include <netinet/in.h>


#include <cstring>
#include <cerrno>


using std::cerr;
using std::string;


namespace dwtest
{
typedef std::vector<string> token_array;

enum {port=1234};

string stderr_sink;

std::set<string> cmd_whitelist;
unsigned cmd_timeout = 0;


class runtime_error: public std::runtime_error
{
public:
    runtime_error(const string& what):
        std::runtime_error(what)
    {}
};


void assert_true(bool b, const string& what=string())
{
    if (!b)
        throw runtime_error(what.empty() ? "assertion failure" : what);
}


namespace posix_result
{
class error: public runtime_error
{
public:
    error(const string& what):
        runtime_error(what + ": " + strerror(errno))
    {}
};

template <typename T>
static void assert_not_neg(T value, const string& what=string())
{
    if (value == -1)
        throw error(string(what.empty() ? "posix result assert" : what));
}
}


class fork
{
public:

    fork():
        detached_(false)
    {
        pid_ = ::fork();

        posix_result::assert_not_neg(pid_, "fork");
    }

    void detach()
    {
        detached_ = true;
    }

    bool is_child()
    {
        return pid_ == 0;
    }

    ~fork()
    {
        if (is_child())
            exit(0);

        else if (detached_)
            return;

        try
        {
            int status;

            posix_result::assert_not_neg(waitpid(pid_, &status, 0), "waitpid");

            cerr << "child " << pid_ << " exit status " << status << "\n";
        }
        catch (runtime_error& error)
        {
            cerr << "child " << pid_ << " wait error, " << error.what() << "\n";

            std::abort();
        }
    }

private:

    fork(const fork&);
    fork& operator=(const fork&);

    pid_t pid_;
    bool detached_;
};


class fd_handler
{
public:

    explicit fd_handler(int fd):
        fd_(fd)
    {
        assert_valid(fd);
    }

    int take_fd()
    {
        int fd = fd_;

        fd_ = -1;

        return fd;
    }

    int fd()
    {
        return fd_;
    }

    ~fd_handler()
    {
        try
        {
            if (fd_ >= 0)
                close(fd_);
        }
        catch (runtime_error& error)
        {
            cerr << "fd handler dtor error, " << error.what() << "\n";
        }
    }

    static void close(int fd)
    {
        assert_valid(fd);
        posix_result::assert_not_neg(::close(fd), "close");
    }

    static void assert_valid(int fd)
    {
        posix_result::assert_not_neg(fd, "assert fd valid");
    }

private:

    fd_handler(const fd_handler&);
    fd_handler& operator=(const fd_handler&);

    int fd_;
};


void dup2(int old_fd, int new_fd)
{
    posix_result::assert_not_neg(::dup2(old_fd, new_fd), "dup2");
}

void sigaction_reset(int signum)
{
    struct sigaction action = {};

    action.sa_handler = SIG_DFL;

    posix_result::assert_not_neg(
        sigaction(signum, &action, NULL), "sigaction reset"
    );
}

void sigaction_nochldwait()
{
    struct sigaction action = {};

    action.sa_handler = SIG_IGN;
    action.sa_flags = SA_NOCLDWAIT;

    posix_result::assert_not_neg(
        sigaction(SIGCHLD, &action, NULL), "sigaction nocldwait"
    );
}

token_array tokenize(const string& line)
{
    std::istringstream stream(line);
    string token;
    token_array result;

    while (stream >> token)
        result.push_back(token);

    return result;
}

std::vector<char*> build_args(token_array& tokens)
{
    std::vector<char*> result;

    result.reserve(tokens.size() + 1);

    for (token_array::iterator i = tokens.begin(); i != tokens.end(); ++i)
        result.push_back(const_cast<char*>((*i).data()));

    result.push_back(static_cast<char*>(0));

    return result;
}

int start_server()
{
    fd_handler fd(socket(AF_INET, SOCK_STREAM, 0));

    struct sockaddr_in addr = {};

    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = INADDR_ANY;

    posix_result::assert_not_neg(
        bind(fd.fd(), (sockaddr*)&addr, sizeof(addr)), "bind"
    );

    posix_result::assert_not_neg(listen(fd.fd(), SOMAXCONN), "listen");

    return fd.take_fd();
}

bool check_cmd_about_to_execute(const string& cmd, const token_array& tokens)
{
    if (cmd_whitelist.find(string(cmd.data())) == cmd_whitelist.end())
    {
        cerr << "cmd " << cmd.data() << " is not in whitelist\n";

        return false;
    }

    cerr << "cmd about to execute: " << cmd.data();

    for (token_array::const_iterator i = tokens.begin(); i != tokens.end(); ++i)
        cerr << ", \"" << *i << "\"";

    cerr << "\n";

    return true;
}

void process_client_input(const string& line)
{
    token_array tokens = tokenize(line);
    assert_true(!tokens.empty(), "empty token array is nonsence");

    const string& cmd = tokens.front();

    if (check_cmd_about_to_execute(cmd, tokens))
    {
        fork cmd_fork;

        if (cmd_fork.is_child())
        {
            try
            {
                fd_handler::close(STDIN_FILENO);

                std::vector<char*> args = build_args(tokens);

                if (cmd_timeout > 0)
                {
                    sigaction_reset(SIGALRM);
                    alarm(cmd_timeout);
                }

                dup2(STDOUT_FILENO, STDERR_FILENO);
                execv(cmd.data(), args.data());
                std::abort();
            }
            catch (runtime_error& error)
            {
                cerr << "client input process error, " << error.what() << "\n";
            }
        }
    }
}

void parse_options(int argc, char* argv[])
{
    int opt;

    enum {log, whitelist, timeout};

    struct option long_opt_list[] = {
        {"log", required_argument, 0, log},
        {"whitelist", required_argument, 0, whitelist},
        {"timeout", required_argument, 0, timeout},
        {0, 0, 0, 0}
    };

    while (
        opt = getopt_long(argc, argv, "", long_opt_list, NULL),
        opt != -1
    )
    {
        switch (opt)
        {
            case log:
                stderr_sink = optarg;
                break;

            case whitelist:
                {
                    std::ifstream src(optarg);
                    std::istream_iterator<string> begin(src), end;

                    cmd_whitelist.insert(begin, end);
                }
                break;

            case timeout:
                cmd_timeout = atoi(optarg);
                break;
        }
    }
}

void detach_session()
{
    try
    {
        fd_handler::close(STDIN_FILENO);
        fd_handler::close(STDOUT_FILENO);

        fd_handler new_stderr(
            open(
                (stderr_sink.empty() ? "/dev/null" : stderr_sink.c_str()),
                O_WRONLY | O_CREAT | O_APPEND | O_NOCTTY,
                S_IRUSR | S_IWUSR | S_IRGRP
            )
        );

        dup2(new_stderr.fd(), STDERR_FILENO);

        posix_result::assert_not_neg(setsid(), "setsid");
    }
    catch (runtime_error& error)
    {
        cerr << "detach session error: " << error.what() << "\n";

        std::abort();
    }
}

void run()
{
    fd_handler server(start_server());
    sigaction_nochldwait();

    while (true)
    {
        fd_handler client(accept(server.fd(), NULL, 0));

        fork client_fork;
        client_fork.detach();

        if (client_fork.is_child())
        {
            try
            {
                dup2(client.fd(), STDIN_FILENO);
                dup2(client.fd(), STDOUT_FILENO);
                sigaction_reset(SIGCHLD);

                string line;

                while (std::getline(std::cin, line))
                    process_client_input(line);
            }
            catch (runtime_error& error)
            {
                cerr << "client servicing error, " << error.what() << "\n";
            }
        }
    }
}
}


int main(int argc, char* argv[])
{
    try
    {
        dwtest::parse_options(argc, argv);

        dwtest::fork daemon_fork;

        daemon_fork.detach();

        if (daemon_fork.is_child())
        {
            dwtest::detach_session();
            dwtest::run();
        }
    }
    catch (dwtest::runtime_error& error)
    {
        cerr << "runtime error, " << error.what() << "\n";
    }
}

