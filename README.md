# README / ЧАВО #

Test project for one famous IT-company.

Тестовое задание, выполненное для одной известной компании.

### What is this repository for? / Что тут к чему ###

Service listening incoming connections on TCP, executes client commands matching white list. Command output is feeded back to client. Execution timeout is provided.

Сервис, слушает входящие соединения по TCP, исполняет приходящие команды в случае, если они находятся в списке разрешённых. Выход выполняемой команды возвращается клиенту. Снабжён таймером принудительного завершения.

### Contribution guidelines / Как бы помочь ###

Do nothing.

Просто не мешайте. Этот проект сделан "для себя", в чисто познавательных целях.
